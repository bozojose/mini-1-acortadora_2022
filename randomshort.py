#!/usr/bin/python3

import webapp
import random
import shelve
from urllib.parse import unquote

urlDic = shelve.open('urlDic')  # diccionario para las URLs con llave un num aleat --> '/recurso': 'URL'
urlDic_inverso = shelve.open('urlDic_inverso')  # dic con clave la URL y valor el num aleat --> 'URL' : '/recurso'
urlDic2 = {}  # Diccionario auxiliar para mostrar la lista de urls en la pagina de inicio


formulario = """
 <form action ="" method ="POST">
    <h2>Introduzca una URL:</h2>
    Url: <input type="text" name="url"><br><br>
    <input type="submit" value="Acortar"><br>
 </form>
"""


class Acortador_URLs(webapp.webApp):
    def parse(self, request):
        method, resource, _ = request.split(' ', 2)
        # Trocea los dos primeros espacios en blanco y
        # luego guarda las 2 primeras palabras
        print(request)
        body = unquote(request.split('\r\n\r\n', 1)[1])  # Trocea por /r/n/r/n solo
        # una vez y acaba (almacena el segundo campo, que sera el body,
        # ya decodificado con utf-8 con unquote)
        return method, resource, body

    def process(self, parsedRequest):
        method, resource, body = parsedRequest
        print('\r\nEl metodo es: ' + method)
        print('El recurso es: ' + resource)
        print('El cuerpo es: ' + body)

        if method == "GET":
            if resource == "/":  # Recurso / (el recurso principal)
                for clave, valor in urlDic.items():  # para cada elemento del dic persistente se
                    # obtienen sus claves y valores para guardarlos en un dic con el que listarlos
                    urlDic2[clave] = valor
                return ("200 OK", "<html><body><h1>Acortador de URLs </h1>" +
                        formulario +
                        "<br><br>" + "URLs almacenadas: "
                        + str(urlDic2) + "</body></html>")
            else:
                if resource in urlDic:  # Si el recurso ya esta como clave, entonces
                    # se hace una redireccion (para URLs ya acortadas)
                    print("HTTP REDIRECT: " + str(urlDic[resource]))
                    return ("302 Found\r\nLocation: " +
                            urlDic[resource], "")  # Redirección
                else:  # Resto de recursos (de localhost)
                    return ("404 NOT FOUND", "<html><body><h1>Recurso no encontrado.</h1>" +
                            "<a href=http://localhost:1234/>Volver</a></body></html>")
        if method == "POST":
            url_qs = body.split("=")[1]  # Sacar url de la qs
            if url_qs == "":  # Con qs vacía en el campo url
                return ("400 Bad Request", "<html><body><h1>Por favor, introduzca una URL.</h1>" +
                        "<a href=http://localhost:1234/>Volver</a></body></html>")
            else:  # Si hay URL para acortar en la qs
                contenido = unquote(url_qs)  # URL extraida de qs decodificada
                print("Cuerpo decodificado: " + contenido)
                # Manejo de inicios de url varios
                if contenido.startswith("http://www"):
                    contenido = "http://" + str(contenido.split("www.")[-1])
                elif contenido.startswith("http://"):
                    contenido = "http://" + str(contenido.split("//")[-1])
                elif contenido.startswith("https://www"):
                    contenido = "https://" + str(contenido.split("www.")[-1])
                elif contenido.startswith("https://"):
                    contenido = "https://" + str(contenido.split("//")[1])
                else:
                    if contenido.startswith("www."):
                        contenido = "https://" +\
                                    str(contenido.split("www.")[-1])
                        # Se tiene en cuenta si empieza
                        # por www. para añadir https://
                    else:
                        contenido = "https://" + contenido
                        # Casos con la url del sitio sin nada delante
                        # (se añade https://)

                if contenido in urlDic_inverso:
                    pass
                else:
                    num_aleatorio = random.randrange(0, 10000000)  # numero aleatorio
                    # para usar como recurso en la URL acortada
                    urlDic["/" + str(num_aleatorio)] = contenido  # Asigna al recurso
                    # aleatorio de la url acortada la URL real
                    urlDic_inverso[contenido] = str(num_aleatorio)  # Asigna a la
                    # URL real la acortada

                return ("200 OK", "<html><body><h1>URL acortada: " +
                        "<a href='" + str(contenido) +
                        "'>" + "http://localhost:1234/" +
                        str(urlDic_inverso[contenido]) + "</a></h1><br><h2>URL original: " +
                        "<a href='" + str(contenido) + "'>" + str(contenido) +
                        "</a></h2><br><br><a href=http://localhost:1234/>Volver</a></body></html>")


if __name__ == "__main__":
    try:
        testWebApp = Acortador_URLs("localhost", 1234)
    except KeyboardInterrupt:
        urlDic.close()
        urlDic_inverso.close()
        print("Cerrando acortador de Urls...")
